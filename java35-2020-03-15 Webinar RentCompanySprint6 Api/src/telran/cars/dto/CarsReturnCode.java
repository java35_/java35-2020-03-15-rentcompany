package telran.cars.dto;

public enum CarsReturnCode {
OK, MODEL_EXISTS, CAR_EXISTS, NO_MODEL, DRIVER_EXISTS, NO_CAR, CAR_REMOVED, CAR_IN_USE, NO_DRIVER
}
