package telran.cars.dto;

public interface ApiConstants {
	String ADD_MODEL = "/model/add";
	
	String ADD_DRIVER = "/driver/add";
	String GET_MODEL = "/model";
	String GET_DRIVER_CARS = "/driver/cars";
	String GET_CAR_MODELS = "/models";
	String GET_MODEL_CARS = "/model/cars";
	String RENT_CAR = "/car/rent";
	String RETURN_CAR = "/car/return";
	String GET_CAR_DRIVERS = "/drivers/car";
	String GET_CAR = "/car";
	String GET_DRIVER = "/driver";
	String ADD_CAR = "/car/add";
	String REMOVE_CAR = "/car/remove";
	
}
