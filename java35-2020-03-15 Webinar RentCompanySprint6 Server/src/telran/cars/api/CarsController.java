package telran.cars.api;

import telran.cars.db.IRentCompany;
import telran.cars.dto.CarsReturnCode;
import telran.cars.dto.Model;
import telran.net.RequestJava;
import telran.net.ResponseJava;
import telran.net.TcpResponseCode;
import telran.net.server.ProtocolJava;

import static telran.cars.dto.ApiConstants.*;

import static telran.net.TcpResponseCode.*;

import java.io.Serializable;

public class CarsController implements ProtocolJava {
	IRentCompany db;
	
	public CarsController(IRentCompany db) {
		this.db = db;
	}

	@Override
	public ResponseJava getResponse(RequestJava request) {
		System.out.println("Incomin message: ");
		System.out.println("Request type: " +  request.requestType);
		System.out.println("Request dat: " +  request.requestData);
		
		switch (request.requestType) {
			case ADD_MODEL:
				return addModel(request.requestData);
			case ADD_CAR:
				return null;
			case ADD_DRIVER:
				return null;
	
			default:
				break;
			}
		return new ResponseJava(WRONG_REQUEST, "Bad request");
		
	}

	private ResponseJava addModel(Serializable requestData) {
		try {
			Model model = (Model) requestData;
			CarsReturnCode returnCode = db.addModel(model);
			System.out.println("Return code: " + returnCode);
			return new ResponseJava(OK, returnCode);
		} catch (Exception e) {
			System.out.println("Wrong request");
			return new ResponseJava(WRONG_REQUEST, e.getMessage());
		}
		
	}

}
