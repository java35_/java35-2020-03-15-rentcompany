package telran.cars.items.clerk;

import java.time.LocalDate;

import telran.cars.items.RentCompanyItem;
import telran.cars.service.IRentCompany;
import telran.view.InputOutput;

public class RentCarItem extends RentCompanyItem {

	public RentCarItem(InputOutput inputOutput, IRentCompany company) {
		super(inputOutput, company);
	}

	@Override
	public String displayedName() {
		
		return "Rent car";
	}

	@Override
	public void perform() {
		String regNumber=getRegNumberExisted();
		if(regNumber==null)return;
		Long licenseId=getLicenseIdExisted();
		if(licenseId==null)
			return;
		String format="dd/MM/yyyy";
		LocalDate rentDate=inputOutput.inputDate("Enter rent date "+format, format);
		if(rentDate==null)
			return;
		Integer rentDays=inputOutput.inputInteger("Enter rent days [1-30],1,30");
		
		inputOutput.outputLine(company.rentCar(regNumber,
				licenseId,
				rentDate,
				rentDays));

	}
	

}
