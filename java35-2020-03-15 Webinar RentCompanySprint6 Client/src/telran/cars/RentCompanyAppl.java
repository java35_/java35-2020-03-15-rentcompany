package telran.cars;

import java.io.IOException;
import java.net.UnknownHostException;

import telran.cars.items.SaveAndExitItem;
import telran.cars.items.clerk.AddDriverItem;
import telran.cars.items.clerk.GetCarsModelItem;
import telran.cars.items.clerk.RentCarItem;
import telran.cars.items.clerk.ReturnCarItem;
import telran.cars.items.driver.GetCarDriversItem;
import telran.cars.items.driver.GetCarItem;
import telran.cars.items.driver.GetDriverItem;
import telran.cars.items.manager.AddCarItem;
import telran.cars.items.manager.AddCarModel;
import telran.cars.items.manager.RemoveCarItem;
import telran.cars.items.manager.RemoveModelItem;
import telran.cars.items.statist.GetMostActiveDriversItem;
import telran.cars.items.statist.GetMostPopularModelsItem;
import telran.cars.items.statist.GetyMostProfitableModelsItem;
import telran.cars.items.technician.GetRecordsItem;
import telran.cars.service.IRentCompany;
import telran.cars.service.RentCompanyProxy;
import telran.view.ConsoleInputOutput;
import telran.view.ExitItem;
import telran.view.InputOutput;
import telran.view.Item;
import telran.view.Menu;
import telran.view.SubMenuItem;


public class RentCompanyAppl {
private static final String COMPANY_FILE = "company.data";
	final static String CONSTANT1 = "const1";
static IRentCompany company;
static InputOutput inputOutput;
	public static void main(String[] args) throws UnknownHostException, IOException {
		inputOutput=new ConsoleInputOutput();
		company = new RentCompanyProxy("localhost", 5000);
		Menu menu=new Menu(getMainMenuItems(), inputOutput);
		menu.runMenu();
		
		String str = CONSTANT1;
		

	}
	private static Item[] getMainMenuItems() {
		Item[]items= {
				new SubMenuItem(getManagerItems(), inputOutput,"Manager" ),
				new SubMenuItem(getClerkItems(), inputOutput, "Clerk"),
				new SubMenuItem(getDriverItems(), inputOutput, "Driver"),
				new SubMenuItem(getStatistItems(), inputOutput, "Statist"),
				new SubMenuItem(getTechnicianItems(), inputOutput, "Technician"),
				new SaveAndExitItem(inputOutput, company, COMPANY_FILE),
				new ExitItem()
		};
		return items;
	}
	private static Item[] getTechnicianItems() {
		Item[] items= {
				new GetRecordsItem(inputOutput, company),
				new ExitItem()};
		return items;
	}
	private static Item[] getStatistItems() {
		Item[] items= {
				new GetMostActiveDriversItem(inputOutput, company),
				new GetMostPopularModelsItem(inputOutput, company),
				new GetyMostProfitableModelsItem(inputOutput, company),
				new ExitItem()};
		return items;
	}
	private static Item[] getDriverItems() {
		Item[]items= {
				new GetCarItem(inputOutput, company),
				new GetDriverItem(inputOutput, company),
				new GetCarDriversItem(inputOutput, company),
				new ExitItem()
		};
		return items;
	}
	private static Item[] getClerkItems() {
		Item[]items= {
				new AddDriverItem(inputOutput, company),
				new GetCarsModelItem(inputOutput, company),
				new RentCarItem(inputOutput, company),
				new ReturnCarItem(inputOutput, company),
				new ExitItem()
		};
		return items;
	}
	private static Item[] getManagerItems() {
		Item[]items= {
			new AddCarModel(inputOutput, company),
			new AddCarItem(inputOutput, company),
			new RemoveCarItem(inputOutput, company),
			new RemoveModelItem(inputOutput, company),
			new ExitItem()
		};
		return items;
	}

}
