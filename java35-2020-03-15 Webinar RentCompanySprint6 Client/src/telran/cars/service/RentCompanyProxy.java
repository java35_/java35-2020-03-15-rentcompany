package telran.cars.service;

import java.io.IOException;
import java.io.Serializable;
import java.net.UnknownHostException;
import java.time.LocalDate;
import java.util.List;

import telran.cars.dto.Car;
import telran.cars.dto.CarsReturnCode;
import telran.cars.dto.Driver;
import telran.cars.dto.Model;
import telran.cars.dto.RemovedCarData;
import telran.cars.dto.RentRecord;
import telran.net.TcpClientJava;

import static telran.cars.dto.ApiConstants.*;

// SomeClass extends AnotherClass
// SomeClass extends AnotherAbstractClass
// SomeClass implements SomeInterface
// SomeClass implements SomeInterface1, SomeInterface2, ...

// SomeInterface extends AnotherInterface


public class RentCompanyProxy extends TcpClientJava implements IRentCompany  {

	public RentCompanyProxy(String hostName, int port) throws UnknownHostException, IOException {
		super(hostName, port);
	}

	@Override
	public int getGasPrice() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setGasPrice(int price) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getFinePercent() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setFinePercent(int finePercent) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public CarsReturnCode addModel(Model model) {
		return sendRequest(ADD_MODEL, model);
	}

	@Override
	public Model getModel(String modelName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CarsReturnCode addCar(Car car) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Car getCar(String regNumber) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CarsReturnCode addDriver(Driver driver) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Driver getDriver(long licenseId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CarsReturnCode rentCar(String regNumber, long licenseId, LocalDate rentDate, int rentDays) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Car> getCarsDriver(long licenseId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Driver> getDriversCar(String regNumber) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Car> getCarsModel(String modelName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RentRecord> getRentRecordsAtDates(LocalDate from, LocalDate to) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RemovedCarData removeCar(String regNumber) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RemovedCarData> removeModel(String modelName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RemovedCarData returnCar(String regNumber, long licenseId, LocalDate returnDate, int damages,
			int tankPercent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getMostPopularCarModels(LocalDate fromDate, LocalDate toDate, int fromAge, int toAge) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getMostProfitableCarModels(LocalDate fromDate, LocalDate toDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Driver> getMostActiveDrivers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getModelNames() {
		// TODO Auto-generated method stub
		return null;
	}

}
